﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {
    /// <summary>
    /// # el3ab internship test
    /// 
    ///   Write code to sortd the circles "children's under gameobject "container" in cirlce form around sort btn when user click on sort button
    /// 
    ///   NOTE : - feel free to change the following code as you see fit if you want 
    ///          - keep you code organized and commented
    /// 
    ///     Good Luck 
    ///   
    /// </summary>

    //container is filled with small circles 
    //  small circles is the game object's  that shall be sorted in form of circle shape around the Button
    public GameObject container; 
	private  float cx, cy , radius , calculatedX ,calculatedY;
	private bool clicked = false;
	private float speed;
	private GameObject sort;

	void Start()
	{

		clicked = false;
		speed = 20;
		sort = GameObject.FindGameObjectWithTag("sortButton");
	}

	void Update()
	{
		// on update rotate the circles around the button
		if (clicked) {

			for (int i = 0; i < container.transform.childCount; i++)
			{
				//  set child position depending on it's index 
				
				container.transform.GetChild(i).transform.RotateAround(sort.transform.position,Vector3.forward,speed*Time.deltaTime);
				
			}
		}
	}

    // onClick on ("sort button") this function will be called and excuted
    public void SortBtn() 
    {
        //loop to scan each child inside the container
        for (int i = 0; i < container.transform.childCount; i++)
        {
            //  set child position depending on it's index 

           container.transform.GetChild(i).transform.localPosition = calculatePosition(i);

        }
		// Adding a little touch ;) :D
		// change sort button to spin button
		// add a different function to onClick SpinBtn()
		sort.GetComponent<Button> ().onClick.AddListener (() => {
			SpinBtn();});
		// Change the text of the button
		sort.GetComponentInChildren<Text> ().text = "Spin";
		// Remove SortBtn() function from the onClick of the button
		sort.GetComponent<Button> ().onClick.RemoveListener (() => {
			SortBtn();});

    }

	public void SpinBtn() 
	{
		// Getting the text of the button
		string text = sort.GetComponentInChildren<Text> ().text;
		// if the text is Stop then transform it to Spin else transform it to Stop
		if (text.Equals("Stop")) {
			clicked = false;
			sort.GetComponentInChildren<Text> ().text = "Spin";

		} else if(text.Equals("Spin")){
			clicked = true;
			sort.GetComponentInChildren<Text> ().text = "Stop";
		}
		
	}

	// function responsible to calculate position 
    // Write your Code Here  
    //VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
    private Vector2 calculatePosition(int index)
    {
		// Get the sort button to know the center of the circle
		GameObject sort = GameObject.FindGameObjectWithTag("sortButton");
		// rect transform used to get the center point (x,y)
		RectTransform rt = (RectTransform)sort.transform;
		cx = rt.rect.center.x;
		cy = rt.rect.center.y;

		radius = 120f;
		float newX = 0, newY=0;
		// Using cartesian algorithm to draw the circle
		// index zero is the first child so choose a first point (x,y) to begin the circle
		if (index == 0) {

			calculatedX = cx;
			calculatedY = radius;
			newX = calculatedX;
			newY = calculatedY;
		}
		// cartesian algorithm draw 8 points at once so here I draw 8 points using modulus
		// each time index is divisible by 8 is time to change calculatedX, calculatedY 
		// that are used to draw new 8 points
		else if(index % 8 == 0 && calculatedX < calculatedY){
			calculatedX +=16;
			calculatedY = Mathf.Sqrt((radius*radius)-(calculatedX*calculatedX));
			newX = cx+ calculatedX;
			newY = cy+ calculatedY;
		}

		/* each if condition is to get one of the 8 points of cartesian algorithm */
		if (index % 8 == 1) {
			newX = cx + calculatedX;
			newY = cy - calculatedY;
		}
		else if (index % 8 == 2) {
			newX = cx - calculatedX;
			newY = cy + calculatedY;
		}
		else if (index % 8 == 3) {
			newX = cx - calculatedX;
			newY = cy - calculatedY;
		}
		else if (index % 8 == 4) {
			newX = cx + calculatedY;
			newY = cy + calculatedX;
		}
		else if (index % 8 == 5) {
			newX = cx + calculatedY;
			newY = cy - calculatedX;
		}
		else if (index % 8 == 6) {
			newX = cx - calculatedY;
			newY = cy + calculatedX;
		}
		else if (index % 8 == 7) {
			newX = cx - calculatedY;
			newY = cy - calculatedX;
		}

		// returning the new points
        return new Vector2(newX, Mathf.Round(newY));
    }


    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    // NOTE AGAIN : you have permission to change code as you see fit if you want 



}
