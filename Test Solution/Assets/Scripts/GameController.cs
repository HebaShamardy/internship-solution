﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
    /// <summary>
    /// # el3ab internship test
    /// 
    ///   Write code to sortd the circles "children's under gameobject "container" in cirlce form around sort btn when user click on sort button
    /// 
    ///   NOTE : - feel free to change the following code as you see fit if you want 
    ///          - keep you code organized and commented
    /// 
    ///     Good Luck 
    ///   
    /// </summary>

    //container is filled with small circles 
    //  small circles is the game object's  that shall be sorted in form of circle shape around the Button
    public GameObject container; 
	private  float cx, cy , radius , calculatedX ,calculatedY;


    // onClick on ("sort button") this function will be called and excuted
    public void SortBtn() 
    {
        //loop to scan each child inside the container
        for (int i = 0; i < container.transform.childCount; i++)
        {
            //  set child position depending on it's index 

           container.transform.GetChild(i).transform.localPosition = calculatePosition(i);

        }
    }


    // function responsible to calculate position 
    // Write your Code Here  
    //VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
    private Vector2 calculatePosition(int index)
    {
		// Get the sort button to know the center of the circle
		GameObject sort = GameObject.FindGameObjectWithTag("sortButton");
		// rect transform used to get the center point (x,y)
		RectTransform rt = (RectTransform)sort.transform;
		cx = rt.rect.center.x;
		cy = rt.rect.center.y;

		radius = 120f;
		float newX = 0, newY=0;
		// Using cartesian algorithm to draw the circle
		// index zero is the first child so choose a first point (x,y) to begin the circle
		if (index == 0) {

			calculatedX = cx;
			calculatedY = radius;
			newX = calculatedX;
			newY = calculatedY;
		}
		// cartesian algorithm draw 8 points at once so here I draw 8 points using modulus
		// each time index is divisible by 8 is time to change calculatedX, calculatedY 
		// that are used to draw new 8 points
		else if(index % 8 == 0 && calculatedX < calculatedY){
			calculatedX +=16;
			calculatedY = Mathf.Sqrt((radius*radius)-(calculatedX*calculatedX));
			newX = cx+ calculatedX;
			newY = cy+ calculatedY;
		}

		/* each if condition is to get one of the 8 points of cartesian algorithm */
		if (index % 8 == 1) {
			newX = cx + calculatedX;
			newY = cy - calculatedY;
		}
		else if (index % 8 == 2) {
			newX = cx - calculatedX;
			newY = cy + calculatedY;
		}
		else if (index % 8 == 3) {
			newX = cx - calculatedX;
			newY = cy - calculatedY;
		}
		else if (index % 8 == 4) {
			newX = cx + calculatedY;
			newY = cy + calculatedX;
		}
		else if (index % 8 == 5) {
			newX = cx + calculatedY;
			newY = cy - calculatedX;
		}
		else if (index % 8 == 6) {
			newX = cx - calculatedY;
			newY = cy + calculatedX;
		}
		else if (index % 8 == 7) {
			newX = cx - calculatedY;
			newY = cy - calculatedX;
		}

		// returning the new points
        return new Vector2(newX, Mathf.Round(newY));
    }


    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    // NOTE AGAIN : you have permission to change code as you see fit if you want 



}
